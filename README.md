# About Auto2X

Auto2X is a WinForms-based C# .NET Framework 4.7.2 application designed to automate the process of upscaling images using the Waifu2x image upscaler API developed by DeepAI @ https://deepai.org/api-docs/#waifu2x.

## Package dependencies

* AForge
* AForge.Video
* AForce.Video.DirectShow
* DeepAI.Client
* Newtonsoft.Json

## User guide

### Setup

* Auto2x will create a preferences file on first launch; use this for configuration. This will be located under `AppData/roaming`. 
* To use Auto2x, you will need to create an API key with DeepAI and add it to the preferences file under `ApiKey`. You can sign up for an account and gain access to a key @ https://deepai.org

### Controls
* The *parameters* pane contains all user controls related to image processing.

#### Image input/output
* *Image Input Directory* refers to where Auto2x should look for images to be upscaled. This must refer to an existing directory, and cannot be the same as the Image Output Directory. Clicking *Default* will reset this field to the value defined in the preferences file.
* *Image Output Directory* refers to where Auto2x should save upscaled images to. This must refer to an existing directory, and cannot be the same as the Image Input Directory. Clicking *Default* will reset this field to the value defined in the preferences file.
* *Use Default Directories* will set both the *Image Input Directory* and *Image Output Directory* to the values defined in the preferences file. 

#### File types/formats
* *Target File Formats - Preferences*: selecting this will force Auto2x to only select and convert images in the target directory ending in the specified formats. These formats can be found and edited in the preferences file.
* *Target File Formats - Any*: selecting this will allow Auto2x to select any image file.

#### Actions
* *Start* will begin the process of upscaling all images in the *Image Input Directory* using the API, with the resulting upscaled images saved to the *Image Output Directory*.
* *Cancel* will stop any in-progress conversions.
* *Reset All* will reset all fields on the application.

#### Output
* The *Output* panel shows the current progress of the application. It updates as images are upscaled, or if issues occur.
* *Open Output Directory* opens the *Image Output Directory* in file explorer.

## Contact

*blizzardsev.dev@gmail.com*