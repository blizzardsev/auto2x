﻿namespace Auto2x
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
			this.textBox_directoryInput = new System.Windows.Forms.TextBox();
			this.groupBox_parameters = new System.Windows.Forms.GroupBox();
			this.button_cancel = new System.Windows.Forms.Button();
			this.checkBox_fileFormatUseAny = new System.Windows.Forms.CheckBox();
			this.checkBox_fileFormatUsePrefs = new System.Windows.Forms.CheckBox();
			this.label_targetedFileFormats = new System.Windows.Forms.Label();
			this.button_useDefaultOutputDirectory = new System.Windows.Forms.Button();
			this.button_useDefaultInputDirectory = new System.Windows.Forms.Button();
			this.button_useDefaultDirectories = new System.Windows.Forms.Button();
			this.button_start = new System.Windows.Forms.Button();
			this.button_resetAll = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textbox_directoryOutput = new System.Windows.Forms.TextBox();
			this.label_directoryInput = new System.Windows.Forms.Label();
			this.button_goToOutputDirectory = new System.Windows.Forms.Button();
			this.groupBox_output = new System.Windows.Forms.GroupBox();
			this.textBox_requestIndicator = new System.Windows.Forms.TextBox();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.toolStrip_options = new System.Windows.Forms.ToolStrip();
			this.toolStripButton_getApiKey = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripButton_openPrefs = new System.Windows.Forms.ToolStripButton();
			this.groupBox_parameters.SuspendLayout();
			this.groupBox_output.SuspendLayout();
			this.toolStrip_options.SuspendLayout();
			this.SuspendLayout();
			// 
			// textBox_directoryInput
			// 
			this.textBox_directoryInput.Location = new System.Drawing.Point(6, 33);
			this.textBox_directoryInput.Name = "textBox_directoryInput";
			this.textBox_directoryInput.Size = new System.Drawing.Size(116, 20);
			this.textBox_directoryInput.TabIndex = 0;
			// 
			// groupBox_parameters
			// 
			this.groupBox_parameters.Controls.Add(this.button_cancel);
			this.groupBox_parameters.Controls.Add(this.checkBox_fileFormatUseAny);
			this.groupBox_parameters.Controls.Add(this.checkBox_fileFormatUsePrefs);
			this.groupBox_parameters.Controls.Add(this.label_targetedFileFormats);
			this.groupBox_parameters.Controls.Add(this.button_useDefaultOutputDirectory);
			this.groupBox_parameters.Controls.Add(this.button_useDefaultInputDirectory);
			this.groupBox_parameters.Controls.Add(this.button_useDefaultDirectories);
			this.groupBox_parameters.Controls.Add(this.button_start);
			this.groupBox_parameters.Controls.Add(this.button_resetAll);
			this.groupBox_parameters.Controls.Add(this.label1);
			this.groupBox_parameters.Controls.Add(this.textbox_directoryOutput);
			this.groupBox_parameters.Controls.Add(this.label_directoryInput);
			this.groupBox_parameters.Controls.Add(this.textBox_directoryInput);
			this.groupBox_parameters.Location = new System.Drawing.Point(13, 29);
			this.groupBox_parameters.Name = "groupBox_parameters";
			this.groupBox_parameters.Size = new System.Drawing.Size(200, 331);
			this.groupBox_parameters.TabIndex = 1;
			this.groupBox_parameters.TabStop = false;
			this.groupBox_parameters.Text = "Parameters";
			// 
			// button_cancel
			// 
			this.button_cancel.Enabled = false;
			this.button_cancel.Location = new System.Drawing.Point(6, 266);
			this.button_cancel.Name = "button_cancel";
			this.button_cancel.Size = new System.Drawing.Size(188, 23);
			this.button_cancel.TabIndex = 13;
			this.button_cancel.Text = "Cancel";
			this.toolTip.SetToolTip(this.button_cancel, "Cancel processing.");
			this.button_cancel.UseVisualStyleBackColor = true;
			this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
			// 
			// checkBox_fileFormatUseAny
			// 
			this.checkBox_fileFormatUseAny.AutoSize = true;
			this.checkBox_fileFormatUseAny.Location = new System.Drawing.Point(9, 163);
			this.checkBox_fileFormatUseAny.Name = "checkBox_fileFormatUseAny";
			this.checkBox_fileFormatUseAny.Size = new System.Drawing.Size(44, 17);
			this.checkBox_fileFormatUseAny.TabIndex = 3;
			this.checkBox_fileFormatUseAny.Text = "Any";
			this.checkBox_fileFormatUseAny.UseVisualStyleBackColor = true;
			this.checkBox_fileFormatUseAny.CheckedChanged += new System.EventHandler(this.checkBox_fileFormatUseAny_CheckedChanged);
			// 
			// checkBox_fileFormatUsePrefs
			// 
			this.checkBox_fileFormatUsePrefs.AutoSize = true;
			this.checkBox_fileFormatUsePrefs.Checked = true;
			this.checkBox_fileFormatUsePrefs.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBox_fileFormatUsePrefs.Location = new System.Drawing.Point(9, 140);
			this.checkBox_fileFormatUsePrefs.Name = "checkBox_fileFormatUsePrefs";
			this.checkBox_fileFormatUsePrefs.Size = new System.Drawing.Size(83, 17);
			this.checkBox_fileFormatUsePrefs.TabIndex = 2;
			this.checkBox_fileFormatUsePrefs.Text = "Preferences";
			this.checkBox_fileFormatUsePrefs.UseVisualStyleBackColor = true;
			this.checkBox_fileFormatUsePrefs.CheckedChanged += new System.EventHandler(this.checkBox_fileFormatUsePrefs_CheckedChanged);
			// 
			// label_targetedFileFormats
			// 
			this.label_targetedFileFormats.AutoSize = true;
			this.label_targetedFileFormats.Location = new System.Drawing.Point(3, 124);
			this.label_targetedFileFormats.Name = "label_targetedFileFormats";
			this.label_targetedFileFormats.Size = new System.Drawing.Size(109, 13);
			this.label_targetedFileFormats.TabIndex = 12;
			this.label_targetedFileFormats.Text = "Targeted File Formats";
			// 
			// button_useDefaultOutputDirectory
			// 
			this.button_useDefaultOutputDirectory.Location = new System.Drawing.Point(128, 72);
			this.button_useDefaultOutputDirectory.Name = "button_useDefaultOutputDirectory";
			this.button_useDefaultOutputDirectory.Size = new System.Drawing.Size(66, 20);
			this.button_useDefaultOutputDirectory.TabIndex = 11;
			this.button_useDefaultOutputDirectory.TabStop = false;
			this.button_useDefaultOutputDirectory.Text = "Default";
			this.toolTip.SetToolTip(this.button_useDefaultOutputDirectory, "Directory to store processed images to.");
			this.button_useDefaultOutputDirectory.UseVisualStyleBackColor = true;
			this.button_useDefaultOutputDirectory.Click += new System.EventHandler(this.button_useDefaultOutputDirectory_Click);
			// 
			// button_useDefaultInputDirectory
			// 
			this.button_useDefaultInputDirectory.Location = new System.Drawing.Point(128, 33);
			this.button_useDefaultInputDirectory.Name = "button_useDefaultInputDirectory";
			this.button_useDefaultInputDirectory.Size = new System.Drawing.Size(66, 20);
			this.button_useDefaultInputDirectory.TabIndex = 10;
			this.button_useDefaultInputDirectory.TabStop = false;
			this.button_useDefaultInputDirectory.Text = "Default";
			this.toolTip.SetToolTip(this.button_useDefaultInputDirectory, "Directory to read images from for processing,");
			this.button_useDefaultInputDirectory.UseVisualStyleBackColor = true;
			this.button_useDefaultInputDirectory.Click += new System.EventHandler(this.button_useDefaultInputDirectory_Click);
			// 
			// button_useDefaultDirectories
			// 
			this.button_useDefaultDirectories.Location = new System.Drawing.Point(6, 98);
			this.button_useDefaultDirectories.Name = "button_useDefaultDirectories";
			this.button_useDefaultDirectories.Size = new System.Drawing.Size(188, 23);
			this.button_useDefaultDirectories.TabIndex = 7;
			this.button_useDefaultDirectories.TabStop = false;
			this.button_useDefaultDirectories.Text = "Use Default Directories";
			this.toolTip.SetToolTip(this.button_useDefaultDirectories, "Use directories configured under preferences.");
			this.button_useDefaultDirectories.UseVisualStyleBackColor = true;
			this.button_useDefaultDirectories.Click += new System.EventHandler(this.button_useDefaultDirectories_Click);
			// 
			// button_start
			// 
			this.button_start.Location = new System.Drawing.Point(6, 237);
			this.button_start.Name = "button_start";
			this.button_start.Size = new System.Drawing.Size(188, 23);
			this.button_start.TabIndex = 5;
			this.button_start.Text = "Start";
			this.toolTip.SetToolTip(this.button_start, "Begin processing.");
			this.button_start.UseVisualStyleBackColor = true;
			this.button_start.Click += new System.EventHandler(this.button_sendRequest_Click);
			// 
			// button_resetAll
			// 
			this.button_resetAll.Location = new System.Drawing.Point(6, 295);
			this.button_resetAll.Name = "button_resetAll";
			this.button_resetAll.Size = new System.Drawing.Size(188, 23);
			this.button_resetAll.TabIndex = 6;
			this.button_resetAll.Text = "Reset All";
			this.toolTip.SetToolTip(this.button_resetAll, "Reset all options.");
			this.button_resetAll.UseVisualStyleBackColor = true;
			this.button_resetAll.Click += new System.EventHandler(this.button_resetAll_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Image Output Directory";
			// 
			// textbox_directoryOutput
			// 
			this.textbox_directoryOutput.Location = new System.Drawing.Point(6, 72);
			this.textbox_directoryOutput.Name = "textbox_directoryOutput";
			this.textbox_directoryOutput.Size = new System.Drawing.Size(116, 20);
			this.textbox_directoryOutput.TabIndex = 1;
			// 
			// label_directoryInput
			// 
			this.label_directoryInput.AutoSize = true;
			this.label_directoryInput.Location = new System.Drawing.Point(6, 17);
			this.label_directoryInput.Name = "label_directoryInput";
			this.label_directoryInput.Size = new System.Drawing.Size(108, 13);
			this.label_directoryInput.TabIndex = 1;
			this.label_directoryInput.Text = "Image Input Directory";
			// 
			// button_goToOutputDirectory
			// 
			this.button_goToOutputDirectory.Location = new System.Drawing.Point(6, 265);
			this.button_goToOutputDirectory.Name = "button_goToOutputDirectory";
			this.button_goToOutputDirectory.Size = new System.Drawing.Size(313, 23);
			this.button_goToOutputDirectory.TabIndex = 7;
			this.button_goToOutputDirectory.Text = "Open Output Directory";
			this.button_goToOutputDirectory.UseVisualStyleBackColor = true;
			this.button_goToOutputDirectory.Click += new System.EventHandler(this.button_goToOutputDirectory_Click);
			// 
			// groupBox_output
			// 
			this.groupBox_output.Controls.Add(this.textBox_requestIndicator);
			this.groupBox_output.Controls.Add(this.button_goToOutputDirectory);
			this.groupBox_output.Location = new System.Drawing.Point(220, 29);
			this.groupBox_output.Name = "groupBox_output";
			this.groupBox_output.Size = new System.Drawing.Size(326, 294);
			this.groupBox_output.TabIndex = 2;
			this.groupBox_output.TabStop = false;
			this.groupBox_output.Text = "Output";
			// 
			// textBox_requestIndicator
			// 
			this.textBox_requestIndicator.AcceptsReturn = true;
			this.textBox_requestIndicator.BackColor = System.Drawing.Color.SandyBrown;
			this.textBox_requestIndicator.Location = new System.Drawing.Point(6, 16);
			this.textBox_requestIndicator.Multiline = true;
			this.textBox_requestIndicator.Name = "textBox_requestIndicator";
			this.textBox_requestIndicator.ReadOnly = true;
			this.textBox_requestIndicator.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox_requestIndicator.Size = new System.Drawing.Size(313, 243);
			this.textBox_requestIndicator.TabIndex = 1;
			this.textBox_requestIndicator.TabStop = false;
			this.textBox_requestIndicator.Text = "Ready";
			// 
			// toolStrip_options
			// 
			this.toolStrip_options.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_getApiKey,
            this.toolStripSeparator1,
            this.toolStripButton_openPrefs});
			this.toolStrip_options.Location = new System.Drawing.Point(0, 0);
			this.toolStrip_options.Name = "toolStrip_options";
			this.toolStrip_options.Size = new System.Drawing.Size(558, 25);
			this.toolStrip_options.TabIndex = 3;
			// 
			// toolStripButton_getApiKey
			// 
			this.toolStripButton_getApiKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButton_getApiKey.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_getApiKey.Image")));
			this.toolStripButton_getApiKey.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_getApiKey.Name = "toolStripButton_getApiKey";
			this.toolStripButton_getApiKey.Size = new System.Drawing.Size(80, 22);
			this.toolStripButton_getApiKey.Text = "Get API key...";
			this.toolStripButton_getApiKey.ToolTipText = "Get an API key for the Waifu2X API.";
			this.toolStripButton_getApiKey.Click += new System.EventHandler(this.toolStripButton_getApiKey_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripButton_openPrefs
			// 
			this.toolStripButton_openPrefs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripButton_openPrefs.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_openPrefs.Image")));
			this.toolStripButton_openPrefs.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripButton_openPrefs.Name = "toolStripButton_openPrefs";
			this.toolStripButton_openPrefs.Size = new System.Drawing.Size(81, 22);
			this.toolStripButton_openPrefs.Text = "Preferences...";
			this.toolStripButton_openPrefs.ToolTipText = "Open the preferences directory.";
			this.toolStripButton_openPrefs.Click += new System.EventHandler(this.toolStripButton_openPrefs_Click);
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(558, 368);
			this.Controls.Add(this.toolStrip_options);
			this.Controls.Add(this.groupBox_output);
			this.Controls.Add(this.groupBox_parameters);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Main";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Auto2x";
			this.groupBox_parameters.ResumeLayout(false);
			this.groupBox_parameters.PerformLayout();
			this.groupBox_output.ResumeLayout(false);
			this.groupBox_output.PerformLayout();
			this.toolStrip_options.ResumeLayout(false);
			this.toolStrip_options.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_parameters;
        private System.Windows.Forms.Label label_directoryInput;
        private System.Windows.Forms.GroupBox groupBox_output;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_goToOutputDirectory;
        private System.Windows.Forms.Button button_resetAll;
        public System.Windows.Forms.TextBox textBox_directoryInput;
        public System.Windows.Forms.TextBox textbox_directoryOutput;
        public System.Windows.Forms.TextBox textBox_requestIndicator;
        private System.Windows.Forms.Button button_useDefaultDirectories;
		private System.Windows.Forms.Button button_useDefaultOutputDirectory;
		private System.Windows.Forms.Button button_useDefaultInputDirectory;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Label label_targetedFileFormats;
		public System.Windows.Forms.CheckBox checkBox_fileFormatUseAny;
		public System.Windows.Forms.CheckBox checkBox_fileFormatUsePrefs;
		private System.Windows.Forms.ToolStrip toolStrip_options;
		private System.Windows.Forms.ToolStripButton toolStripButton_openPrefs;
		private System.Windows.Forms.Button button_cancel;
		private System.Windows.Forms.ToolStripButton toolStripButton_getApiKey;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
	}
}

