﻿namespace Auto2x
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Drawing;
	using System.IO;
	using System.Linq;
	using System.Net;
	using System.Threading;
	using System.Threading.Tasks;
	using System.Windows.Forms;
	using DeepAI;
    using Newtonsoft.Json;

	public partial class Main : Form
    {
        /// <summary>
        /// Gets the SessionPreferences; preferences for the current session.
        /// </summary>
        private Preferences SessionPreferences { get; set; }

        /// <summary>
        /// Gets the ProgramPath; used referentially to load preferences, or save a new set if none exist.
        /// </summary>
        private string ProgramPath { get; set; }

        /// <summary>
        /// Cancellation token for backing out of conversion process if needed.
        /// </summary>
        private CancellationTokenSource SessionCancellationToken { get; set; }

        /// <summary>
        /// Initialises a new instance of the <see cref="Main"/> class.
        /// Attempts to load configuration if it exists, or set it up otherwise.
        /// </summary>
        public Main()
        {
            InitializeComponent();
            ProgramPath = $"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}/Auto2x";
            SessionCancellationToken = new CancellationTokenSource();

            if (!Directory.Exists(ProgramPath))
            {
                // Create the preferences file if the program path doesn't exist yet
                Directory.CreateDirectory(ProgramPath);
                File.WriteAllText($"{ProgramPath}/preferences.json", Properties.Resources.preferences);
                MessageBox.Show(
                    $"Auto2x directory has been created at: '{ProgramPath}'.{Environment.NewLine}" +
                    $"Please refer to preferences.json for program configuration.",
                    "Auto2x - Setup completed");
            }

            try
            {
                // Attempt to load preferences from disk
                SessionPreferences = JsonConvert.DeserializeObject<Preferences>(File.ReadAllText($"{ProgramPath}/preferences.json"));
            }
            catch
            {
                // Failed to load preferences; prompt the user for recreation from default
                // Optional as user may have preferences they want to retain
                var createPreferencesFile = MessageBox.Show(
                    $"'{ProgramPath}/preferences.json' could not be found, or contains invalid JSON." +
                    $"{Environment.NewLine}{Environment.NewLine}Default preference values will be used." +
                    $"{Environment.NewLine}{Environment.NewLine}Would you like to create a new preferences file?",
                    "Auto2x - Failed to load preferences",
                    MessageBoxButtons.YesNo);

                if (createPreferencesFile == DialogResult.Yes)
				{
                    File.WriteAllText($"{ProgramPath}/preferences.json", Properties.Resources.preferences);
                    MessageBox.Show(
                        $"Auto2x directory has been created at: '{ProgramPath}'.{Environment.NewLine}" +
                        $"Please refer to preferences.json for program configuration.",
                        "Auto2x - Preferences file created");
                }

                SessionPreferences = new Preferences(
                    "",
                    "C:/Auto2x/In/",
                    "C:/Auto2x/Out/",
                    new List<string>()
                    {
                        ".jpg",
                        ".jpeg",
                        ".bmp"
                    });
            }

            var fileTypeLabel = " (";
            foreach (var prefFileType in SessionPreferences.AcceptedFileTypes)
            {
                fileTypeLabel += $" {prefFileType} ";
            }
            checkBox_fileFormatUsePrefs.Text += $"{fileTypeLabel})";
            textBox_directoryInput.Text = SessionPreferences.DefaultInputPath;
            textbox_directoryOutput.Text = SessionPreferences.DefaultOutputPath;
        }

        /// <summary>
        /// Attempts to make a request to the Waifu2x API for each file in <paramref name="files"/>, downloading the result to the <paramref name="outputDirectory"/>.
        /// </summary>
        /// <param name="files">The list of files to upscale.</param>
        /// <param name="outputDirectory">The directory to save upscaled files from the API to.</param>
        /// <param name="cancellationToken">Cancellation token for aborting the process early.</param>
        /// <returns></returns>
        private async Task UpscaleAndDownloadImages(
            List<FileInfo> files,
            DirectoryInfo outputDirectory,
            CancellationToken cancellationToken)
		{
            await Task.Run(() =>
            {
				var api = new DeepAI_API(SessionPreferences.ApiKey);
				foreach (var file in files)
				{
                    try
					{
						if (cancellationToken.IsCancellationRequested)
						{
                            // Back out if the cancellation token is invoked
							MessageBox.Show(
								"Operation cancelled.",
								"Auto2x - Operation cancelled");
                            button_start.Invoke((MethodInvoker)(() => button_start.Enabled = true));
                            button_cancel.Invoke((MethodInvoker)(() => button_cancel.Enabled = false));
                            SetStatusIndicators(StatusTypes.Ready);
                            return;
						}

                        // Make request, save response, write to log
						var response = api.callStandardApi(
							"waifu2x",
							new
							{
								image = file.OpenRead(),
								scale = "2x",
								denoise = "2"
							});

						using (var webClient = new WebClient())
						{
							webClient.DownloadFile(response.output_url, $"{outputDirectory.FullName}/auto2x_{DateTime.Now.GetHashCode()}.png");
						}
                        AddLogItem($"File {file.Name} converted successfully.");
					}
					catch (Exception exception)
					{
                        AddLogItem($"Failed to convert file {file.Name}: {exception.Message}");
                    }
				}

                button_start.Invoke((MethodInvoker)(() => button_start.Enabled = true));
                button_cancel.Invoke((MethodInvoker)(() => button_cancel.Enabled = false));
                SetStatusIndicators(StatusTypes.Ready);
                return;
			});
        }

        /// <summary>
        /// Adds a new text line to the request indicator display.
        /// </summary>
        /// <param name="text">The text to write to the display.</param>
        private void AddLogItem(string text)
		{
            textBox_requestIndicator.Invoke((MethodInvoker)(() => textBox_requestIndicator.Text += $"{Environment.NewLine}{text}"));
		}

        /// <summary>
        /// Sets the application title text and request indicator text based on the given <paramref name="statusType"/>.
        /// </summary>
        /// <param name="statusType">The current status of the application.</param>
        private void SetStatusIndicators(StatusTypes statusType)
		{
            var statusText = "";
            switch (statusType)
			{
                case StatusTypes.Ready:
                    statusText = "Ready";
                    break;

                case StatusTypes.InProgress:
                    statusText = "Working";
                    break;

                case StatusTypes.Complete:
                    statusText = "Done";
                    break;

                case StatusTypes.Cancelling:
                    statusText = "Cancelling";
                    break;

                case StatusTypes.Error:
                    statusText = "Failed";
                    break;
            }

            ActiveForm.Invoke((MethodInvoker)(() => ActiveForm.Text = $"Auto2x - {statusText}"));
            AddLogItem(statusText);
        }

        /// <summary>
        /// Take each file in the Input directory and send a scale-up request to Auto2x.
        /// Download request output from Auto2x link and deposit in the Output directory with a new filename.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private async void button_sendRequest_Click(object sender, EventArgs e)
        {
            // Begin the operation
            DirectoryInfo inputDirectory, outputDirectory;

            if (textBox_directoryInput.Text.Contains(".") || textbox_directoryOutput.Text.Contains("."))
            {
                // Cancel if the input/output directories are files
                MessageBox.Show(
                    "Input/output directory cannot be a file.",
                    "Auto2x - Operation failed");
            }
            else if (textBox_directoryInput.Text == textbox_directoryOutput.Text)
            {
                // Cancel if the input/output directories are identical
                MessageBox.Show(
                    "Input and output directories cannot be identical.",
                    "Auto2x - Operation failed");
                return;
            }

            try
            {
                inputDirectory = new DirectoryInfo($"{textBox_directoryInput.Text}");
                outputDirectory = new DirectoryInfo($"{textbox_directoryOutput.Text}");
            }
            catch
            {
                // Cancel if target directories don't exist
                MessageBox.Show(
                    "Invalid directory specified.",
                    "Auto2x - Operation failed");

                return;
            }

            try
            {
                List<FileInfo> validFiles = new DirectoryInfo(textBox_directoryInput.Text).GetFiles().ToList();
                if (checkBox_fileFormatUsePrefs.Checked)
                {
                    // Remove any files where the prefs indicate not to include it
                    validFiles.RemoveAll(file => !SessionPreferences.AcceptedFileTypes.Contains(file.Extension));
                }

                if (validFiles.Count == 0)
				{
                    // Cancel if no valid files to convert after checks
                    MessageBox.Show(
                        "No files to convert.",
                        "Auto2x - Operation failed");

                    return;
                }

                // Set up UI, reset cancellation token and begin the process
                SetStatusIndicators(StatusTypes.InProgress);
                button_start.Enabled = false;
                button_cancel.Enabled = true;
                SessionCancellationToken = new CancellationTokenSource();
                UpscaleAndDownloadImages(validFiles, outputDirectory, SessionCancellationToken.Token);
            }
            catch
            {
                MessageBox.Show(
                    $"Could not read from/write to the specified directories.{Environment.NewLine}" +
                    "Ensure available R/W/E permissions are in place, or choose different directories.",
                    "Auto2x - Operation failed");
                return;
            }
        }

        /// <summary>
        /// Cancels an in-progress conversion.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_cancel_Click(object sender, EventArgs e)
        {
            SetStatusIndicators(StatusTypes.Cancelling);
            button_cancel.Enabled = false;
            SessionCancellationToken.Cancel();
        }

        /// <summary>
        /// Resets all application fields.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_resetAll_Click(object sender, EventArgs e)
        {
            SetStatusIndicators(StatusTypes.Ready);
            textBox_requestIndicator.BackColor = Color.SandyBrown;

            textBox_directoryInput.Text = string.Empty;
            textbox_directoryOutput.Text = string.Empty;
        }

        /// <summary>
        /// Resets the default input directory field to value from preferences.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_useDefaultInputDirectory_Click(object sender, EventArgs e)
        {
            textBox_directoryInput.Text = SessionPreferences.DefaultInputPath;
        }

        /// <summary>
        /// Resets the default output directory field to value from preferences.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_useDefaultOutputDirectory_Click(object sender, EventArgs e)
        {
            textbox_directoryOutput.Text = SessionPreferences.DefaultOutputPath;
        }

        /// <summary>
        /// Resets both input and output directory fields to values from preferences.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_useDefaultDirectories_Click(object sender, EventArgs e)
        {
            textBox_directoryInput.Text = SessionPreferences.DefaultInputPath;
            textbox_directoryOutput.Text = SessionPreferences.DefaultOutputPath;
        }

        /// <summary>
        /// Opens the output directory location in Explorer.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        private void button_goToOutputDirectory_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(@"C:/Windows/Explorer.exe", textbox_directoryOutput.Text.Replace('/', '\\') + Path.DirectorySeparatorChar);
            }
            catch
            {
                MessageBox.Show(
                    "Invalid directory specified.",
                    "Auto2x - Operation failed");
                return;
            }
        }

        /// <summary>
        /// Prevents selection of both any file format and preferences file format options.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
		private void checkBox_fileFormatUsePrefs_CheckedChanged(object sender, EventArgs e)
		{
            if (checkBox_fileFormatUseAny.Checked && checkBox_fileFormatUsePrefs.Checked)
            {
			    checkBox_fileFormatUseAny.Checked = false;
            }
		}

        /// <summary>
        /// Warn the user that .PNG files will be converted when the any file format option is selected, as the
        /// denoise/upscale is most effective on formats susceptible to compression damage such as JPG.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
		private void checkBox_fileFormatUseAny_CheckedChanged(object sender, EventArgs e)
		{
            if (checkBox_fileFormatUsePrefs.Checked && checkBox_fileFormatUseAny.Checked)
            {
                checkBox_fileFormatUsePrefs.Checked = false;
                MessageBox.Show(
                    ".PNG files will be targeted for conversion. These may already be high quality and may not gain from conversion.",
                    "Auto2x - .PNG file warning");
            }
			
		}

        /// <summary>
        /// Opens the output preferences location in Explorer.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
		private void toolStripButton_openPrefs_Click(object sender, EventArgs e)
		{
            Process.Start(@"C:/Windows/Explorer.exe", ProgramPath.Replace('/', '\\') + Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Opens the default browser, directing the user to the DeepAI homepage.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
		private void toolStripButton_getApiKey_Click(object sender, EventArgs e)
		{
            Process.Start("https://deepai.org");
		}
	}
}