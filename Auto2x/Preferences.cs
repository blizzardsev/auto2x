﻿namespace Auto2x
{
	using System.Collections.Generic;

	/// <summary>
	/// Helper class for preferences management from file.
	/// </summary>
	public class Preferences
	{
		/// <summary>
		/// Gets the Api key used to connect to Waifu2x.
		/// </summary>
		public string ApiKey { get; private set; }

		/// <summary>
		/// Gets or sets the AcceptedFileTypes.
		/// </summary>
		public string DefaultInputPath { get; set; }

		/// <summary>
		/// Gets or sets the AcceptedFileTypes.
		/// </summary>
		public string DefaultOutputPath { get; set; }

		/// <summary>
		/// Gets or sets the AcceptedFileTypes.
		/// </summary>
		public List<string> AcceptedFileTypes { get; set; }

		/// <summary>
		/// Initialises a new instance of the <see cref="Preferences"/> class.
		/// </summary>
		/// <param name=""></param>
		/// <param name="defaultInputPath">The default path to select files for conversion from.</param>
		/// <param name="defaultOutputPath">The default path to deposit files from conversion to.</param>
		/// <param name="acceptedFileTypes">The list of file types to run conversion for.</param>
		public Preferences(
			string apiKey,
			string defaultInputPath,
			string defaultOutputPath,
			List<string> acceptedFileTypes)
		{
			ApiKey = apiKey;
			DefaultInputPath = defaultInputPath;
			DefaultOutputPath = defaultOutputPath;
			AcceptedFileTypes = acceptedFileTypes;
		}
	}
}