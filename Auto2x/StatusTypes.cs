﻿namespace Auto2x
{
	public enum StatusTypes
	{
		Ready,
		InProgress,
		Complete,
		Cancelling,
		Error
	}
}